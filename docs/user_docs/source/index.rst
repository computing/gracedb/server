.. GraceDB documentation master file, created by
   sphinx-quickstart on Mon Jun 15 09:22:37 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===================================
GraceDB Documentation
===================================

Contents:

.. toctree::
   :maxdepth: 2

   ref_manual
   Documentation for the ligo-gracedb client package <https://ligo-gracedb.readthedocs.io/>
   LIGO/Virgo Public Alert Guide <https://emfollow.docs.ligo.org/userguide/>
   Report a bug (LVK users) <https://git.ligo.org/computing/gracedb/server/-/issues>


