# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2019-08-09 18:21
from __future__ import unicode_literals

from django.db import migrations

AUTHGROUP_INFO = {
    'name': 'grb_managers',
    'description': ('LIGO/Virgo members in the GRB subgroup who have '
        'permission to update external GRB events in GraceDB'),
}


def create_authgroup(apps, schema_editor):
    AuthGroup = apps.get_model('ligoauth', 'AuthGroup')
    Permission = apps.get_model('auth', 'Permission')

    # Create AuthGroup
    ag = AuthGroup.objects.create(**AUTHGROUP_INFO)

    # Get permissions for "T90"-ing a grbevent
    perm = Permission.objects.get(
        content_type__app_label='events',
        codename='t90_grbevent'
    )

    # *IMPORTANT*: clear all current users who have this
    # permission.  It should only be assigned to groups
    # going forward.
    perm.user_set.clear()

    # Add this permission to the new AuthGroup
    ag.permissions.add(perm)


def delete_authgroup(apps, schema_editor):
    AuthGroup = apps.get_model('ligoauth', 'AuthGroup')

    # Delete AuthGroup
    ag = AuthGroup.objects.get(name=AUTHGROUP_INFO['name'])
    ag.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0049_update_access_managers_membership'),
    ]

    operations = [
        migrations.RunPython(create_authgroup, delete_authgroup),
    ]
