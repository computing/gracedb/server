# Generated by Django 3.2.20 on 2023-12-04 17:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0086_add_aframe_pipeline'),
    ]

    operations = [
        migrations.AddField(
            model_name='mlyburstevent',
            name='detection_statistic',
            field=models.FloatField(null=True),
        ),
        migrations.AddIndex(
            model_name='mlyburstevent',
            index=models.Index(fields=['detection_statistic'], name='events_mlyb_detecti_b8f225_idx'),
        ),
    ]
